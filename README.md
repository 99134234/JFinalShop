# JFinalShop
JFinaShop轻量级商城系统,极速开发,支持手机端和PC端和小程序,让您轻松打造自己的独立商城，同时也方便二次开发，让您快速搭建个性化独立商城，为您节约更多时间，去陪恋人、家人和朋友。

技术框架

MVC：JFinal 3.6

页面:enjoy

缓存:ehcache

数据库Mysql

功能有：

（1）商品管理（商品分类、商品参数、商品管理）

（2）文章管理(文章分类、文章管理)

（3）会员管理(会员管理)

（4）订单管理(订单管理)

（4）营销管理(广告、广告位、友情链接)

（5）系统管理(角色、管理员、存储插件、地区、设置、模板、清除缓存)

官网地址: [http://www.jrecms.com](http://www.jrecms.com)

体验地址: 前台:http://shop.jrecms.com   后台:http://shop.jrecms.com/admin/login 账号:admin 123456


 联系QQ：644080923 交流群:575535321


小程序端：


<img src="https://images.gitee.com/uploads/images/2019/0326/165424_50c58c2e_623319.jpeg"  height="330">

<img src="https://images.gitee.com/uploads/images/2019/0326/165440_93809373_623319.jpeg"  height="330">

<img src="https://images.gitee.com/uploads/images/2019/0326/165440_93809373_623319.jpeg"  height="330">

<img src="https://images.gitee.com/uploads/images/2019/0326/165449_d0c33fcf_623319.jpeg"  height="330">

<img src="https://images.gitee.com/uploads/images/2019/0326/165503_bddcb65a_623319.jpeg"  height="330">

<img src="https://images.gitee.com/uploads/images/2019/0326/165513_d51d2ad9_623319.jpeg"  height="330">

<img src="https://images.gitee.com/uploads/images/2019/0326/165524_e9acaf5b_623319.jpeg"  height="330">

<img src="https://images.gitee.com/uploads/images/2019/0326/165534_1e25debe_623319.jpeg"  height="330">


PC端:

![输入图片说明](https://gitee.com/uploads/images/2018/0329/112459_63640c64_623319.png "091fb920ee72bb79705cb32173cfb924.png")

手机端:

![输入图片说明](https://gitee.com/uploads/images/2018/0329/112514_d895dfac_623319.png "eb9ada18c3b3b239d11a76fe9c348048.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0329/112522_e614b646_623319.png "ed6843ffbb5ae6518e3380b0a3790c20.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0329/112534_9bd7d9a9_623319.png "c4c82574b0b8d590c6cca70ed837d18a.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0329/112543_b1e12126_623319.png "90eb2f3bebf88ca349a299e49eed3d4d.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0329/112550_88681c84_623319.png "微信图片_20180329112318.png")